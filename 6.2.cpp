#include <iostream>
#include <thread>
#include <future>
#include <vector>
#include <numeric>
#include <algorithm>
#include <ctime>
#include <random>
#include <chrono> 

using namespace std;

// find the largest elemet using single thread
template <typename Iterator, class Compare>
Iterator  find_max(Iterator first, Iterator last, Compare comp) {
	Iterator res = first;
	for (Iterator it = first; it != last; advance(it, 1)) {
		if (comp(*it, *res))
			res = it;
	}
	return res;
}


// return the iterator of the largest element using multiple threads
template <typename Iterator, class Compare>
Iterator  async_find_max(Iterator first, Iterator last, Compare comp) {
	unsigned long const length = std::distance(first, last);
	if (!length)
		return last;

	// calculate the number of threads to use
	unsigned long const min_per_thread = 25;
	unsigned long const max_threads = (length + min_per_thread - 1) / min_per_thread;
	unsigned long const hardware_threads = std::thread::hardware_concurrency();
	unsigned long const num_threads
		= std::min(hardware_threads != 0 ? hardware_threads : 2, max_threads);
	cout << "Number of threads: " << num_threads << endl;


	unsigned long const block_size = length / num_threads;
	vector<future<Iterator>> futures(num_threads - 1);

	// allocate the threads
	Iterator block_start = first;
	for (unsigned long i = 0; i < (num_threads - 1); i++) {
		Iterator block_end = block_start;
		advance(block_end, block_size);
		futures[i] = async(find_max<Iterator, Compare>, block_start, block_end, comp);
		block_start = block_end;
	}

	// find the max in the future results
	Iterator res = find_max(block_start, last, comp);
	for (unsigned long i = 0; i < (num_threads - 1); i++) {
		Iterator temp = futures[i].get();
		if (comp(*temp, *res))
			res = temp;
	}
	return res;
}



int main() {
	vector<int> foo;
	for (int i = 0; i < 1000000; i++) {
		foo.push_back(i);
	}

	// rearrange the elements in the array
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	shuffle(foo.begin(), foo.end(), std::default_random_engine(seed));


	// find the max using single thread
	double start_time1 = clock();
	int res1 = *find_max(foo.begin(), foo.end(), [](int &a, int &b) {return a > b; });
	double end_time1 = clock();

	// find the max using multiple threads
	double start_time2 = clock();
	int res2 = *async_find_max(foo.begin(), foo.end(), [](int &a, int &b) {return a > b; });
	double end_time2 = clock();

	cout << "Single thread function finds the max element is " << res1 << endl;
	cout << "Single thread function takes " << (end_time1 - start_time1) / CLOCKS_PER_SEC << " seconds" << endl;
	cout << "Multiple threads function finds the max element is " << res1 << endl;
	cout << "Multiple threads function takes " << (end_time2 - start_time2) / CLOCKS_PER_SEC << " seconds" << endl;

}