#include <iostream>
#include <future>
#include <ctime>

using namespace std;

// original Fibonacci number
unsigned fib(unsigned n)
{
	if (n == 0)
		return 0;
	if (n == 1)
		return 1;
	return fib(n - 1) + fib(n - 2);
}


// asynchronous fibonacci number
unsigned async_fib(unsigned n)
{
	if (n == 0)
		return 0;
	if (n == 1)
		return 1;
	future<unsigned> f1 = async(async_fib, n-1);
	unsigned f2 = async_fib(n - 2);
	return f2 + f1.get();
}

int main() {
	// test the performance of original code
	double begin_time1 = clock();
	unsigned res1 = fib(30);
	double end_time1 = clock();
	cout << "Using simple code, we know the 30th Fibonacci number is: " << res1 << endl;
	cout << "Simple code takes " << (end_time1 - begin_time1) / CLOCKS_PER_SEC << " seconds" << endl;

	// test the performance of asynchronous code
	double begin_time2 = clock();
	unsigned res2 = async_fib(30);
	double end_time2 = clock();
	cout << "Using simple code, we know the 30th Fibonacci number is: " << res2 << endl;
	cout << "Simple code takes " << (end_time2 - begin_time2) / CLOCKS_PER_SEC << " seconds" << endl;

}