## README ##
NAME: Meng Wang
This is my assignment for homework 6.

### 6.1 ###
6.1.cpp is my source code for problem 6.1. 

To do a test, we use the code to find the 30th Fibonacci number. The original simple takes only 0.039 seconds, but the asynchronous code takes unreasonable time. That's because when calculating the Fibonacci number asynchronously, we have opened too many threads. The number of threads increases expotentially, which takes unreasonable time.


### 6.2 ###
6.2.cpp is my source code for problem 6.2. It is a template version.
